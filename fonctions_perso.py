import pandas as pd

"""Permet de faire une requête SQL en 1 ligne"""
def sql_request(request):
    # On importe sqlalchemy et pymysql, qui nous permettra de communiquer avec la base de données
    from sqlalchemy import create_engine
    import pymysql
    import traceback #Pour pouvoir print les traceback de l'exception levée comme si l'erreur avait eu lieu

    # On crée un moteur de connexion à la base de données
    # Voici les informations dont vous pourrez avoir besoin
    # user : python
    # password : pythonPFE2018
    # hote : pfeensai2018.cx4hfyhhu5xc.eu-central-1.rds.amazonaws.com
    # port : 3306
    # base de données : football
    #engine = create_engine("mysql+pymysql://python:pythonPFE2018@pfeensai2018.cx4hfyhhu5xc.eu-central-1.rds.amazonaws.com:3306/football")
    engine = create_engine("mysql+pymysql://python:pythonPFE2018@data4sports.cx4hfyhhu5xc.eu-central-1.rds.amazonaws.com:3306/football")
    #http://data4sports.cx4hfyhhu5xc.eu-central-1.rds.amazonaws.com/
    # On crée une connexion
    conn = engine.connect()
        
    # Dans un try: except: finally:
    # On essaye de
    try:
        # Lire les 100 premieres lignes de la table tracking et les stocker dans un dataframe
        result = pd.read_sql(request, conn)
        
        
    # Si on n'y arrive pas
    except Exception as e:
        traceback.print_exc()
        
    # Mais quoi qu'il arrive (succes ou echec)
    finally:
        # On ferme la connexion à la base de données    
        conn.close()
        # On ferme le moteur de connexion
        engine.dispose()
        return(result)

    
    
    
"""
La fonction prend en entrée un match et normalise les positions.
"""
def cgtpos(match):
    if match[(match.time_sec == 0.0) 
       & (match.ht == '1' )
       & (match.team_id == match.team_id.unique()[0] )].posx.iloc[0] > 0 :
            xft1 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[0] )].posx * -1
            xst1 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[0] )].posx
            xft2 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[1] )].posx
            xst2 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[1] )].posx * -1
            yft1 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[0] )].posy * -1
            yst1 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[0] )].posy
            yft2 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[1] )].posy
            yst2 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[1] )].posy * -1
    else :
            xft1 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[0] )].posx 
            xst1 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[0] )].posx * -1
            xft2 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[1] )].posx * -1
            xst2 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[1] )].posx 
            yft1 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[0] )].posy 
            yst1 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[0] )].posy * -1
            yft2 = match[(match.ht == '1' ) & (match.team_id == match.team_id.unique()[1] )].posy * -1
            yst2 = match[(match.ht == '2' ) & (match.team_id == match.team_id.unique()[1] )].posy 
    posx1 = xft1.append(xft2)
    posx2 = xst1.append(xst2)
    posx = posx1.append(posx2)
    posy1 = yft1.append(yft2)
    posy2 = yst1.append(yst2)
    posy = posy1.append(posy2)
    posx = posx + abs(min(posx))
    posx = round((posx*100)/max(posx),1)
    posy = posy + abs(min(posy))
    posy = round((posy*100)/max(posy),1)
    del match['posx']
    del match['posy']
    match = pd.concat([match,posx],axis=1)
    match = pd.concat([match,posy],axis=1)
    return(match)



"""
La fonction prend en entrée une table tracking complète et renvoit la table triée: changement des positions et suppressions des dixième de seconde. 
"""
def trans_track(track):
    track = track[track.time_sec  % 1 == 0]
    newdf = pd.DataFrame(columns=list(track.columns.values))
    for i in range(0,len(track.match_id.unique())): #-1 car il n'y a pas la deuxieme équipe du dernier match
        match = track[track.match_id == track.match_id.unique()[i]]
        match = cgtpos(match)
        newdf = pd.concat([newdf,match], axis=0, sort=False)
    
    del newdf['date_id']
    return(newdf)


"""
Prend en entrée la table d'events complète et renvoie une table avec 1 ligne par tir
"""
def filter_events(events):
    events = events.rename(columns = {"period_id":"ht",
                                      "MIN":"min",
                         "SEC":"sec",
                         "Name (Event Types)":"type",
                         "Outcome":"outcome",
                         "X Coordinates":"x",
                         "Y Coordinates":"y",
                         "Qualifier Type":"qualifier_type",
                         "Description":"description",
                         "Qualifier Value":"qualifier_value"})
    result = (events
     .query("type in ['Miss', 'Post', 'Attempt Saved', 'Goal']")
     .drop(["outcome", "qualifier_type", "description", "qualifier_value", "date_id"], axis=1)
     .groupby(by="event_id")
     .agg({"ht": lambda x: x.mode(),
           "min": lambda x: x.mode(),
       "sec": lambda x: x.mode(),
       "type": lambda x: x.mode(),
       "x": "median",
       "y": "median",
       "match_id": lambda x: x.mode(),
       "name_id": lambda x: x.mode(),
       "team_id": lambda x: x.mode(),
       "event_id": lambda x: x.mode()})
     .assign(goal=lambda df: df["type"].replace(["Attempt Saved", "Miss", "Post", "Goal"], [False, False, False, True]))
     .drop(["type"], axis=1)
             )
    return(result)


"""
Prend en entrée une table events et convertit le temps en seconde.
"""
def convertisseur(events):
    events['time_sec'] = 0
    index = events[events.ht==1].index
    events.loc[index, 'time_sec'] = events.loc[index, 'min']*60.0 + events.loc[index, 'sec']
    
    index1 = events[events.ht==2].index
    events.loc[index1, 'time_sec'] = events.loc[index1, 'min']*60.0 + events.loc[index1, 'sec'] - 2700.0 
        
    del events['min']
    del events['sec']
    return(events)



"""
Créer la clé de jointure pour les tables events et track (et change les index). Attention c'est une liste en sortie.
"""
def key_join_events(events):
    events.index = range(events.shape[0])
    
    events.ht = events.ht.astype('str')
    time_events = events.time_sec.astype('str').str.split('.', expand=True)[0]
    id_merge = events.match_id.str.split('_', expand=True)[1] + '_' + events.ht + '_' + time_events
    events['id_merge'] = id_merge
    
    return(events)

"""
Créer la clé de jointure pour la table track (et change les index).
"""
def key_join_track(track):
    track.index = range(track.shape[0])

    time_track = track.time_sec.astype('str').str.split('.', expand=True)[0]
    id_merge = track.match_id.str.split('_', expand=True)[1] + '_' + track.ht + '_' + time_track
    track['id_merge'] = id_merge
    
    return(track)

"""
Ajoute la variable booléenne: gardien ou non à une table track.
"""
def add_gardien(track):
    track['gardien'] = False
    indexg = track[track.name_id.isin(['Player_0040','Player_0034','Player_0013','Player_0043','Player_0042','Player_0011','Player_0080'])].index
    track.loc[indexg, 'gardien'] = True
    return(track)

"""
Prépare la table events (appelle fonctions précédentes).
"""
def prepa_events(events):
    events = filter_events(events)
    events = convertisseur(events)
    events = key_join_events(events)
    
    return(events)

"""
Prépare la table events (appelle fonctions précédentes).
"""
def prepa_track(track):
    track = trans_track(track)
    track = key_join_track(track)
    track = add_gardien(track)
    
    return(track)

"""
Prends en entrée les tables issues de prepa_track et prepa_events, et renvoie une table avec 22 lignes par tir (1 par joueur tracké)
"""
def shots_info(track, events):
    return (pd.merge(track, events, how="right", on="id_merge")
            .drop(["time_sec_y",
                   "ht_y",
                   "match_id_y"], axis=1)
            .rename(columns = {"time_sec_x": "time",
                               "ht_x": "half_time",
                               "posx": "player_x",
                               "posy": "player_y",
                               "name_id_x": "player",
                               "team_id_x": "player_team",
                               "match_id_x": "match",
                               "x": "shot_x",
                               "y": "shot_y",
                               "name_id_y": "striker",
                               "team_id_y": "striker_team"
                              }
                   )
           )
    
print("slt gro")