from fonctions_perso import sql_request
import pandas as pd
import numpy as np 
import warnings
warnings.filterwarnings("ignore")

'''
Ce fichier contient 4 fonctions et des paramètres :

- make_stat : Constitue les stats sur les phasesde possession autour du temps entrée
- get_position : Récupère la position exacte associée à un événement
- where_is_ballon_incomplet : Constitue une BDD des positions connues du ballon 
- where_is_ballon : Renvoie à tout instant la position du ballon à l'aide d'interpolation linéaire
- run_shot : Lance make_stat pour un tir

run comprend également un attribut débuggage renvoyant le (les pour deux ht) tableau(s) intermédiaire(s) construit(s) par build_fitness

'''

def get_position(match_id, period_id, name_id, minute, sec):
    minute = minute - 45*(int(period_id) - 1)
    time_sec = minute*60 + sec + 0.0
    result = sql_request("SELECT * FROM tracking WHERE match_id LIKE '%s' AND ht LIKE '%s' AND name_id LIKE '%s' AND time_sec = %s" % (match_id, period_id, name_id, str(time_sec)))
    return result.iloc[0]

def where_is_ballon_incomplet(match_id, period_id, minute, seconde):
    # Importation des événements et récupération des positions exactes du ballon
    #events = sql_request("SELECT DISTINCT event_id, match_id, name_id, team_id, MIN, SEC, period_id FROM events WHERE match_id LIKE '%s' AND period_id LIKE '%s' AND MIN <= %s AND MIN > %s ORDER BY event_id ASC" % (match_id, period_id,str(minute+1),str(min(minute-5,0))))
    events = sql_request("SELECT * FROM events WHERE match_id LIKE '%s'"% (match_id))
    events = events.rename(columns = {"MIN":"minu",
                                     "SEC":"sec",
                                     "Name (Event Types)":"type",
                                     "Outcome":"outcome",
                                     "X Coordinates":"x",
                                     "Y Coordinates":"y",
                                     "Qualifier Type":"qualifier_type",
                                     "Description":"description",
                                     "Qualifier Value":"qualifier_value"})

    events = events[events.type != "Player on"]
    events = events[events.type != "Player Off"]
    events = events[events.type != "Card"]
    events = events[events.type != "Deleted Event"]
    
    events = events.loc[:,["event_id", "match_id", "name_id", "team_id", "minu", "sec", "period_id"]].drop_duplicates()
    events = events[events.period_id == int(period_id)]
    minute = minute + 45*(int(period_id) - 1)
    events = events[events.minu <= minute+1]
    events = events[events.minu > min(minute-5,0)]
    return events.apply(lambda df: get_position(df.match_id, df.period_id, df.name_id, df.minu, df.sec), axis=1)

#Fonction renvoyant à tout moment la position du ballon    
def where_is_ballon(match_id, period_id, minute, seconde):
    time_sec_r = minute * 60 + seconde
    pos_ballon = where_is_ballon_incomplet(match_id, period_id, minute, seconde).sort_values(by=['time_sec'])
    x1 = pos_ballon[pos_ballon.time_sec <= time_sec_r].iloc[0].posx
    y1 = pos_ballon[pos_ballon.time_sec <= time_sec_r].iloc[0].posy
    t1 = pos_ballon[pos_ballon.time_sec <= time_sec_r].iloc[0].time_sec
    x2 = pos_ballon[pos_ballon.time_sec >= time_sec_r].iloc[0].posx
    y2 = pos_ballon[pos_ballon.time_sec >= time_sec_r].iloc[0].posy
    t2 = pos_ballon[pos_ballon.time_sec >= time_sec_r].iloc[0].time_sec
    
    if (t1 != t2):
        ya = (y2 - y1)/(t2 - t1) 
        yb = y1 - ya*t1
        yt = yb + ya*time_sec_r
        xa = (x2 - x1)/(t2 - t1) 
        xb = x1 - xa*t1
        xt = xb + xa*time_sec_r
        return xt,yt
    else :
        return x1,y1
    
def make_stat(match_id, period_id, minute, seconde):
    # Séparation des phases de possession
    pos_ballon = where_is_ballon_incomplet(match_id, period_id, minute, seconde).sort_values(by=['time_sec'])
    team = list(set(pos_ballon.team_id))[0]

    poss_team1 = (pos_ballon.team_id == team).astype(np.float32).cumsum()
    poss_team2 = (pos_ballon.team_id != team).astype(np.float32).cumsum()

    pos_ballon['poss_id']=None
    pos_ballon.loc[:,'poss_id'][pos_ballon.team_id == team] = poss_team2
    pos_ballon.loc[:,'poss_id'][pos_ballon.team_id != team] = poss_team1
    pos_ballon.poss_id = pos_ballon.apply(lambda df : df.team_id+"_"+str(int(df.poss_id)), axis = 1)
    
    #Agrégation par phase de possession
    poss_gby = pos_ballon.groupby('poss_id')
    stat_poss = pd.DataFrame([],columns=[])
    stat_poss['poss_nb_pass']  = poss_gby.ht.count()
    stat_poss['poss_time_min_sec'] = poss_gby.time_sec.min()
    stat_poss['poss_time_max_sec'] = poss_gby.time_sec.max()
    stat_poss['poss_duree_sec'] = poss_gby.time_sec.max() - poss_gby.time_sec.min()
    stat_poss['poss_freq_pass_sec'] = (poss_gby.time_sec.max() - poss_gby.time_sec.min()) / poss_gby.ht.count()
    diff_x = poss_gby.apply(lambda df: df.posx.diff().fillna(0.).apply(lambda x : (x*0.01)**2))
    diff_y = poss_gby.apply(lambda df: df.posy.diff().fillna(0.).apply(lambda x : (x*0.01)**2))
    diff_t = poss_gby.apply(lambda df: df.time_sec.diff().fillna(1.))
    stat_poss['poss_dist_m'] = (diff_x/diff_t + diff_y/diff_t).apply(lambda x : x**(0.5)).groupby('poss_id').sum()
    stat_poss['poss_vit_ms'] = stat_poss['poss_dist_m']/stat_poss['poss_duree_sec']
    
    return stat_poss


def run_shot(match_id, period_id, minute, seconde):
    minute = minute - 45*(int(period_id)-1)
    stat_poss = make_stat(match_id, period_id, minute, seconde)
    retour = stat_poss
    try :
        if retour[stat_poss.poss_time_max_sec == minute*60+seconde].shape[0] == 1 :
            return retour[stat_poss.poss_time_max_sec == minute*60+seconde]
        elif retour[stat_poss.poss_time_max_sec > minute*60+seconde][stat_poss.poss_time_min_sec < minute*60+seconde].shape[0] == 1 :
            return retour[stat_poss.poss_time_max_sec > minute*60+seconde][stat_poss.poss_time_min_sec < minute*60+seconde]
        else :
            return pd.DataFrame([[np.nan, 
                                  np.nan,
                                  np.nan, 
                                  np.nan, 
                                  np.nan, 
                                  np.nan, 
                                  np.nan]], 
                                columns=["poss_nb_pass", 
                                         "poss_time_min_sec", 
                                         "poss_time_max_sec", 
                                         "poss_duree_sec",
                                         "poss_freq_pass_sec",
                                         "poss_dist_m",
                                         "poss_vit_ms"])
    except:
        return pd.DataFrame([[np.nan, 
                              np.nan,
                              np.nan, 
                              np.nan, 
                              np.nan, 
                              np.nan, 
                              np.nan]], 
                            columns=["poss_nb_pass", 
                                     "poss_time_min_sec", 
                                     "poss_time_max_sec", 
                                     "poss_duree_sec",
                                     "poss_freq_pass_sec",
                                     "poss_dist_m",
                                     "poss_vit_ms"])