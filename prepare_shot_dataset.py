import math

import pandas as pd


def filter_shots(event_database):
    """Filter the event database to keep only shot events and useful columns.
    Also add a boolean 'goal' column.
    
    :param event_database: The event database. Must contain columns :
        'period_id'
        'MIN'
        'SEC'
        'Name (Event Types)'
        'X Coordinates'
        'Y Coordinates'
        'match_id'
        'name_id'
        'team_id'
        'event_id'

    :type event_database: pandas.DataFrame
    :return: the filtered database with a new 'goal' column
    :rtype: pandas.DataFrame
    """

    result = (
        event_database.rename(
            columns={
                "MIN": "min",
                "SEC": "sec",
                "Name (Event Types)": "type",
                "X Coordinates": "x",
                "Y Coordinates": "y",
            }
        )
        .loc[
            :,
            [
                "period_id",
                "min",
                "sec",
                "type",
                "x",
                "y",
                "match_id",
                "name_id",
                "team_id",
                "event_id",
            ],
        ]
        .query("type in ['Miss', 'Post', 'Attempt Saved', 'Goal']")
        .groupby(by="event_id")
        .agg(
            {
                "period_id": lambda x: x.mode(),
                "min": lambda x: x.mode(),
                "sec": lambda x: x.mode(),
                "type": lambda x: x.mode(),
                "x": "median",
                "y": "median",
                "match_id": lambda x: x.mode(),
                "name_id": lambda x: x.mode(),
                "team_id": lambda x: x.mode(),
                "event_id": lambda x: x.mode(),
            }
        )
        .assign(
            goal=lambda df: df.loc[:, "type"].replace(
                ["Attempt Saved", "Miss", "Post", "Goal"], [False, False, False, True]
            )
        )
        .drop(["type"], axis=1)
    )
    return result


def add_instant_id(result):
    """Create a new id column which identifies a specific instant in a specific match.
    i.e, 2 events with the same instant_id occured at the same moment in the same match.
    
    :param result: a table processed with the 'filter_shots' function.
    :type result: pandas.DataFrame
    :return: the table with a new 'instant_id' column
    :rtype: pandas.DataFrame
    """

    new_column = result.loc[:, "match_id"]
    new_column = new_column.str.slice(6)
    new_column = new_column + "_" + result.loc[:, "period_id"].map(str)
    time_sec = result.loc[:, "min"] * 60 + result.loc[:, "sec"]
    time_sec = time_sec.map(str)
    new_column = new_column + "_" + time_sec
    return result.assign(instant_id=lambda df: new_column)


def preprocess_events(event_table):
    """keep only shots and create instant_id
    
    :param event_table: raw event data from SQL database
    :type event_table: pandas.DataFrame
    :return: ready to merge shot database
    :rtype: pandas.DataFrame
    """

    result = filter_shots(event_table)
    result = add_instant_id(result)
    return result


def preprocess_tracking(tracking_table):
    """filter lines with non-integer timestamps and create instant_id
    
    :param event_table: raw tracking data from SQL database
    :type event_table: pandas.DataFrame
    :return: ready to merge positions database
    :rtype: pandas.DataFrame
    """
    result = tracking_table.drop(["date_id"], axis=1)
    result = result.loc[result["time_sec"].map(lambda x: x.is_integer())]
    result = result.rename(columns={"ht": "period_id", "posx": "x", "posy": "y"})

    new_column = result.loc[:, "match_id"]
    new_column = new_column.str.slice(6)
    new_column = new_column + "_" + result.loc[:, "period_id"].map(str)
    new_column = new_column + "_" + result.loc[:, "time_sec"].map(int).map(str)

    result = result.assign(instant_id=lambda df: new_column)

    return result


def merge_events_tracking(events, tracking):
    """Merge the 2 tables to get the positions of each player for each shot.
    
    :param events: preprocessed events table
    :type events: pandas.DataFrame
    :param tracking: preprocessed tracking table
    :type tracking: pandas.DataFrame
    :return: the merge on instant_id
    :rtype: pandas.DataFrame
    """
    result = pd.merge(events, tracking, on="instant_id")
    result = result.drop(["period_id_y", "match_id_y"], axis=1)
    result = result.rename(
        columns={
            "period_id_x": "period_id",
            "x_x": "x_opta",
            "y_x": "y_opta",
            "match_id_x": "match_id",
            "name_id_x": "striker_id",
            "team_id_x": "striker_team",
            "x_y": "x_raw",
            "y_y": "y_raw",
            "name_id_y": "player_id",
            "team_id_y": "team_id",
        }
    )
    return result


def distance(x1, x2, y1, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def is_goalkeeper(player_id):
    gk_list = pd.read_csv("goalkeepers.csv")
    return player_id in gk_list


def add_boundaries(track):
    return track.groupby("match_id").apply(
        lambda df: df.assign(
            min_x=lambda df2: min(df2.loc[:, "raw_x"]),
            max_x=lambda df2: max(df2.loc[:, "raw_x"]),
            min_y=lambda df2: min(df2.loc[:, "raw_y"]),
            max_y=lambda df2: max(df2.loc[:, "raw_y"]),
        )
    )
