from fonctions_perso import sql_request
import pandas as pd
import numpy as np 
import warnings
warnings.filterwarnings("ignore")

'''
Ce fichier contient 4 fonctions et des paramètres :

- run : permet de construire la table fitness pour un (joueur, match, mitemps, minu, sec) donné
- build_fitness : permet de construire une base de travail utile à la fonction run
- stat_acc : calcule sur les phases d'accélération
- stat_course : culcule sur les phases de courser

run comprend également un attribut débuggage renvoyant le (les pour deux ht) tableau(s) intermédiaire(s) construit(s) par build_fitness

'''


# Paramètres
ms_kmh = 60*60/1000
sec_min = 1/60
seuil_vit = 25
seuil_acc = 2

# Fonction permettant de calculer les stats sur les phases d'accélération
def stat_acc(fitness):
    acc_gby = fitness.groupby('acc_id')
    pic_acc = acc_gby['acc'].max()
    duree   = acc_gby['time_sec'].max() - acc_gby['time_sec'].min()
    # Calcul de la distance valable que parce que "intervalle de temps régulier"
    dist = acc_gby['vit_abs_kmh'].sum() * 0.1 / 3600
    
    retour = pd.concat([pic_acc,duree,dist], axis=1)
    retour.columns = ['pic_acc','duree',"dist"]
    
    return retour[retour.index > 0][retour.pic_acc >= 3][retour.duree > 1]

# Fonction permettant de calculer les stats sur les phases de courses
def stat_course(fitness):
    
    vit_gby = fitness.groupby('course_id')
    vit_min_kmh = vit_gby['vit_abs_kmh'].min()
    vit_max_kmh = vit_gby['vit_abs_kmh'].max()
    
    duree   = vit_gby['time_sec'].max() - vit_gby['time_sec'].min()
    # Calcul de la distance valable que parce que "intervalle de temps régulier"
    dist = vit_gby['vit_abs_kmh'].sum() * 0.1 / 3600
    
    retour = pd.concat([vit_min_kmh,vit_max_kmh,duree,dist], axis=1)
    retour.columns = ['vit_min_kmh','vit_max_kmh','duree',"dist"]
    
    #On filtre pour ne garder que les phases dépassant le seuil
    #On retire course_id = 0 car il correspond au cas où le joueur ne dépasse pas les 25kmh
    retour = retour[retour.index > 0][retour.duree >= 1]
    #print(retour)
    return retour

def build_fitness(fitness, course_id_ht1 = 0, acc_id_ht1 = 0):
    #Les positions sont en cm au départ. Il convient de les mettre en m (x0.01)
    #L'intervalle de temps est de 0.1 sec. 
    #Le coeff. multiplicatif est de 0.1 pour les vitesses
    fitness.loc[:,'vitx']=fitness.posx.diff()*0.1
    fitness.loc[:,'vity']=fitness.posy.diff()*0.1
    fitness.loc[:,'accx']=fitness.vitx.diff()/0.1
    fitness.loc[:,'accy']=fitness.vity.diff()/0.1

    fitness.loc[:,'vit_abs_kmh']=np.sqrt(fitness.vitx**2 + fitness.vity**2) * ms_kmh
    fitness.loc[:,'acc']=fitness.vit_abs_kmh.diff()/0.1

    acc_beginning=(fitness.acc >= seuil_acc).astype(np.float32).cumsum()
    acc_ending=(fitness.acc < seuil_acc).astype(np.float32).cumsum()  

    fitness['acc_id']=None
    fitness.loc[:,'acc_id'][acc_beginning==acc_ending] = 0
    fitness.loc[:,'acc_id'][acc_beginning!=acc_ending] = acc_ending[acc_beginning!=acc_ending]+acc_id_ht1
    
    course_beginning = (fitness.vit_abs_kmh >= seuil_vit).astype(np.float32).cumsum()
    course_ending = (fitness.vit_abs_kmh < seuil_vit).astype(np.float32).cumsum() 

    fitness['course_id']=None
    fitness.loc[:,'course_id'][course_beginning==course_ending] = 0
    fitness.loc[:,'course_id'][course_beginning!=course_ending] = course_ending[course_beginning!=course_ending]+course_id_ht1
    return fitness
    
def make_stat(label,fitness):
    
    statacc = stat_acc(fitness)
    statcourse = stat_course(fitness)
    
    retour = np.zeros(10)
    retour[0] = len(statacc)
    retour[4] = len(statcourse)
    
    if len(fitness) != 0 :
        retour[8] = sum(fitness.vit_abs_kmh.fillna(0.)/3600)*0.1
        retour[9] = len(fitness.time_sec)*0.1/60 
    
        if len(statacc) != 0 : 
            retour[1] = np.median(statacc.duree)
            retour[2] = max(statacc.duree)
            retour[3] = sum(statacc.dist)

        if len(statcourse) != 0 :
            retour[5] = np.median(statcourse.duree)
            retour[6] = max(statcourse.duree)
            retour[7] = sum(statcourse.dist)
        
    retour = pd.DataFrame([retour], index = [label], columns = ['fit_acc_nb',\
                                                          'fit_acc_time_med',\
                                                          'fit_acc_time_max',\
                                                          'fit_acc_dist_km',\
                                                          'fit_cour_nb',\
                                                          'fit_cour_time_med',\
                                                          'fit_cour_time_max',\
                                                          'fit_cour_dist_km',\
                                                          'fit_dist_km',\
                                                          'fit_time_played'])
    
    return retour

# Fonction principale, c'est la fonction à appeler. Elle utilise les fonctions auxiliaires
# Les valeurs par défaut permettent de pouvoir faire des requêtes sur tout le match
def run (match, joueur, time = ('2', 120, 20), debugage = False):
    mitemps = time[0]
    if mitemps == '2':
        minu = time[1]-45
    else:
        minu = time[1]
    sec = time[2]
    
    fitness = sql_request("SELECT * FROM tracking WHERE match_id = \'"+ match +"\' AND name_id = \'" + joueur + "\'")

    # Construction du label de la stat
    if minu == 120-45 :
        # Cas où l'utilisateur a demandé des stats sur le match complet
        label = match[6:] + "_" + joueur[7:]
    else :
        label = match[6:] + "_" + joueur[7:] + "_" + mitemps + "_" + str(minu) + "'" +str(sec)
        
    # Gestion des mi-temps et du temps de jeu    
    if mitemps == '1' :
        fitness = build_fitness(fitness.loc[(fitness.ht == '1') & (fitness.time_sec < (minu*60 + sec)) ])
    
    if mitemps == '2' :
        fitness1 = build_fitness(fitness.loc[fitness.ht == '1'])
        try : 
            course_id_max = max(fitness1.course_id)+1
            acc_id_max = max(fitness1.acc_id)+1
        except : 
            course_id_max = 0
            acc_id_max = 0
        
        fitness2 = build_fitness(fitness.loc[(fitness.ht == '2') & (fitness.time_sec < (minu*60 + sec)) ], \
                                 course_id_max, \
                                 acc_id_max)
        fitness = pd.concat([fitness1,fitness2])
        
    retour = make_stat(label,fitness)
        
    if debugage :
        return retour,fitness
    else :
        return retour